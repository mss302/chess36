package chess;


/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */
public abstract class Piece {
	private boolean moved = false;
	private String name;
	private boolean white;
	
	public String getName() {
		return name;
	}
	
	public void setName(String in) {
		name = in;
		
	}
	public boolean getMoved(){
		return moved;
	}
	public void move(){
		moved = true;
	}
	public abstract boolean validMove(int s1,int s2, int f1, int f2);
	
	public String toString(){
		return name;
	}

	public void setColor(boolean b) {
		white = b;
		
	}

	public boolean getColor() {
		return white;
		
	}

}
