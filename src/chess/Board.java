package chess;
import pieces.*;

import java.util.ArrayList;
import java.util.Scanner;
/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */
public class Board {
	/**
	 * Board asks pieces if they can physically move in that direction and then sees if its is ok
	 * for the actual board at the time. It also handles all of the special cases such as
	 * en passant and castling.
	 */
	int[] hiddenEnPass = new int[15];
	private Piece[][] theBoard;
	public Board(){
		/**
		 * @theBoard is our 2d array of pieces that prints out and acts as the physical
		 * board in which we can move pieces around
		 */
		theBoard = new Piece[8][8];
		for(int i = 0; i<theBoard.length; i++){
			for(int j = 0; j<theBoard[i].length; j++){
				theBoard[i][j] = null;
			}
		}
		//pawns
		//white 
		for(int a = 0; a<=7; a++){
			theBoard[a][1] = new Pawn('w');
			theBoard[a][6] = new Pawn('b');
		}
		
		
		//rook
		theBoard[0][0] = new Rook('w');
		theBoard[7][0] = new Rook('w');
		theBoard[0][7] = new Rook('b');
		theBoard[7][7] = new Rook('b');
		//kings
		theBoard[4][0] = new King('w');
		theBoard[4][7] = new King('b');
		
		//queens
		theBoard[3][0] = new Queen('w');
		theBoard[3][7] = new Queen('b');
		//bishop
		theBoard[2][0] = new Bishop('w');
		theBoard[5][0] = new Bishop('w');
		theBoard[2][7] = new Bishop('b');
		theBoard[5][7] = new Bishop('b');
		//knight
		theBoard[1][0] = new Knight('w');
		theBoard[6][0] = new Knight('w');
		theBoard[1][7] = new Knight('b');
		theBoard[6][7] = new Knight('b');
		
		//white
		for(int i = 0; i<theBoard.length; i++){
			for(int j = 0; j<=1; j++){
				theBoard[i][j].setColor(true);
			}
		}
		for(int i = 0; i<theBoard.length; i++){
			for(int j = 6; j<theBoard[i].length; j++){
				theBoard[i][j].setColor(false);
			}
		}
		
		
	}
	public String toString(){
		/**
		 * @toString prints the board appropriately
		 */
		String output = "";

		for(int i = theBoard.length-1; i>=0; i--){
			for(int j = 0; j<theBoard.length; j++){
				if(theBoard[j][i] != null)
					output += theBoard[j][i] + " ";
				else{
					if((i%2 == 0 && j%2 == 0) || (i%2 == 1 && j%2 == 1))
						output += "## ";
					else
						output += "   ";
				}
					
			}
			output += i+1;
			output += "\n";
		}
		output += " a  b  c  d  e  f  g  h";
		return output;
	}
	//0 no check
	//1 white check
	//2 white checkmate  
	//3 black check
	//4 black checkmate
	public int getCheckmate(boolean whiteturn){
		/**
		 * @getCheckmate checks to see if you are in check. then sees if you can get out.
		 * if you cannot get out, it is checkmate or stalemate and the game is over.
		 */
		
		ArrayList<Point> murderPath = new ArrayList<Point>();
		ArrayList<Point> murderers = new ArrayList<Point>();
		
		//find the kings
		int returncodeW = 0;
		int returncodeB = 0;
		
		int wk1 = -1;
		int wk2 = -1;
		int bk1 = -1;
		int bk2 = -1;
		for(int i = 0; i<theBoard.length; i++){
			for(int j = 0; j<theBoard[i].length; j++){
				if(theBoard[i][j] != null){
					if(theBoard[i][j].getName().equals("wK")){
						wk1 = i;
						wk2 = j;
					}
					else if(theBoard[i][j].getName().equals("bK")){
						bk1 = i;
						bk2 = j;
					}
						
				}
				if(wk1 != -1 && bk1 != -1)
					break;
			}
			if(wk1 != -1 && bk1 != -1)
				break;
		}
		//kings have been found
		//look for check
		for(int i = 0; i<theBoard.length; i++){
			for(int j = 0; j<theBoard[i].length; j++){
				if(theBoard[i][j] != null){
					if(theBoard[i][j].getColor() && specialmovescheck(i,j,bk1,bk2)){
						Point p = new Point();
						p.x = i;
						p.y = j;
						
						murderers.add(p);
						returncodeB = 1;
					}
					
						
				}
				
			}
			
		}
		if(!whiteturn && returncodeB == 1)
			return 3;
		//look for check
		for(int i = 0; i<theBoard.length; i++){
			for(int j = 0; j<theBoard[i].length; j++){
				if(theBoard[i][j] != null){
					if(!theBoard[i][j].getColor() && specialmovescheck(i,j,wk1,wk2)){
							Point p = new Point();
							p.x = i;
							p.y = j;
							murderers.add(p);
							returncodeW = 1;
					}
				}
			}
		}
		//fill the murder path
	
		
		
		
		if(whiteturn && returncodeW == 1)
			return 1;
		//TODO checkmate for the returncode whos in check
		if(returncodeW == returncodeB)
			return 0;
		int s1,s2;
		if(returncodeW == 1)
		{
			s1 = wk1;
			s2 = wk2;
		}
		else{
			s1 = bk1;
			s2 = bk2;
		}
		ArrayList<Point> adjpoints = new ArrayList<Point>();
		for(Point suspect : murderers){
			Point temp = new Point();
			int xdif = Math.abs(s1-suspect.x);
			int ydif = Math.abs(s2-suspect.y);
			int xincrement = 0;
			int yincrement = 0;
			if(xdif!=0)
				xincrement = (suspect.x-s1)/xdif;
			if(ydif!=0)
				yincrement = (suspect.y-s2)/ydif;
			
			if(!((xdif == 2 && ydif == 1 )||(xdif == 1 && ydif == 2 ))){
				temp = new Point();
				temp.x = s1+xincrement;
				temp.y = s2+yincrement;
				adjpoints.add(temp);
				
				for(int looks= 1 ; looks <= Math.max(xdif, ydif); looks++){
					
					temp = new Point();
					temp.x = s1+looks*xincrement;
					temp.y = s2+looks*yincrement;
					murderPath.add(temp);
					
						
				}
			}
		}
		
		int kt1,kt2;
		Point kt = new Point();
		for(int q = -1; q<= 1; q++){
			for(int r = -1; r<= 1; r++){
				kt1 = q+s1;
				kt2 = r+s2;
				if(kt1 >= 0 && kt1 <= 7 && kt2 >= 0 && kt2 <= 7 && theBoard[kt1][kt2]==null){
					kt.x=kt1; kt.y = kt2;
					boolean match = true;
				
					for(Point adjcentpoints: adjpoints){
						if(!(adjcentpoints.x == kt1 && adjcentpoints.y==kt2)){
							if(returncodeB == 1)
								return 3;
							return returncodeW;
							
						}
					}
					
						
						
					}
				}
				
			}
		
		
		//TODO ONLY IF 1 path!! check if there is a path from your pieces to path
		boolean color = returncodeW==1;
		if(murderers.size() == 1){
			for(Point p: murderPath)
				for(int i = 0; i<theBoard.length; i++){
					for(int j = 0; j<theBoard[i].length; j++){
						if(theBoard[i][j] != null && i!=s1 && j!=s2){
							//TODO get the color
					
							if(theBoard[i][j].getColor()==color && specialmovescheck(i,j,p.x,p.y)){
								if(returncodeB == 1)
									return 3;
								return 1;
								
							}
	
						}
					}
				}
		}
		if(returncodeB == 1)
			return 4;
		return 2;
	}
	private boolean specialmovescheck(int s1, int s2, int f1, int f2){
		boolean pbool = theBoard[s1][s2].validMove(s1,s2,f1,f2);
		
		//pawns cant go forward kill
		if(theBoard[s1][s2].getName().equals("bp") || theBoard[s1][s2].getName().equals("wp")){
			if(theBoard[f1][f2]!=null){
				if(s1==f1){
					
					return false;
				}
			}
			if(theBoard[f1][f2]==null){
				if(s1 != f1){
					
					return false;
				}
			}
		}
		if(theBoard[s1][s2].getName().equals("wK") || theBoard[s1][s2].getName().equals("bK")){
			if(s1-f1==-2 || s1-f1 == 2)
				return false;
		}
		//pawns cant diag to null
		//kings only move 1
		
		return pbool; 
		
	}
	
	public boolean move(int s1,int s2,int f1, int f2, boolean whiteturn) {
		/**
		 * @move actually moves the pieces around.
		 */
		//isthereanythingats1s2
		for(int i =0;i<hiddenEnPass.length;i++){
			if(whiteturn&&i<8){
				hiddenEnPass[i]=0;
			}else if(!whiteturn&&i>=8){
				hiddenEnPass[i]=0;
			}
		}
		
	
		
		
		
		
		if(theBoard[s1][s2] == null){
			System.out.println("The piece you are moving is null \n");
			return false;
		}
		
		//get color check
		if(whiteturn != theBoard[s1][s2].getColor()){
			System.out.println("The piece you are trying to move is not yours \n");
			return false;
		}
			
		//valid motion

		
		if(!theBoard[s1][s2].validMove(s1,s2,f1,f2)){

			
			System.out.println("the move is not a valid move \n");
			return false;
		}
		
		//isthereanythingatf1f2 or between (!knight)
		/**
		 * checks to see if there is anything between where you are and where 
		 * you want to go.
		 */
		int xdif = Math.abs(s1-f1);
		int ydif = Math.abs(s2-f2);
		int xincrement = 0;
		int yincrement = 0;
		if(xdif!=0)
			xincrement = (f1-s1)/xdif;
		if(ydif!=0)
			yincrement = (f2-s2)/ydif;
		
		if(!((xdif == 2 && ydif == 1 )||(xdif == 1 && ydif == 2 ))){
			
			for(int looks= 1 ; looks <= Math.max(xdif, ydif)-1; looks++){
				
				if(theBoard[s1+looks*xincrement][s2+looks*yincrement]!=null){
					System.out.println("There is a piece blocking where you want to move \n");
					return false;
				}
					
			}
		}
		
		if(theBoard[s1][s2].getName().equals("wp")){
			if(theBoard[f1][f2]!=null){
				if(s1==f1){
					System.out.println("Pawns can't move straight into pieces \n");
					return false;
				}
			}
		}
		if(theBoard[s1][s2].getName().equals("bp")){
			if(theBoard[f1][f2]!=null){
				if(s1==f1){
					System.out.println("Pawns can't move straight into pieces \n");
					return false;
				}
			}
		}
		
		int skip =0;
		/**
		 * en passant stuff
		 */
		if(theBoard[f1][f2]== null || whiteturn != theBoard[f1][f2].getColor()){
			if(theBoard[s1][s2].getName().equals("wp")||theBoard[s1][s2].getName().equals("bp")){
				if(theBoard[f1][f2]==null&&((s1-f1==1||s1-f1==-1)&&(s2-f2==1||s2-f2==-1))&&((hiddenEnPass[f1+8]==0&&whiteturn)||(hiddenEnPass[f1]==0&&!whiteturn))){
					//pawn going diagonal

					return false;
				}else if(theBoard[f1][f2]==null&&((s1-f1==1||s1-f1==-1)&&(s2-f2==1||s2-f2==-1))&&((hiddenEnPass[f1+8]==1&&whiteturn)||(hiddenEnPass[f1]==1&&!whiteturn))){

					skip = 1;
					if(theBoard[s1][s2].getName().equals("wp")){

						theBoard[f1][f2]=theBoard[s1][s2];
						theBoard[s1][s2]=null;
						theBoard[f1][f2-1] = null;
					}else if(theBoard[s1][s2].getName().equals("bp")){

						theBoard[f1][f2]=theBoard[s1][s2];
						theBoard[s1][s2]=null;
						theBoard[f1][f2+1] = null;
					}
				}
			}
			/**
			 * promotion stuff
			 */
			if(theBoard[s1][s2]!=null){
				if(theBoard[s1][s2].getName().equals("wp")){
					
					if(f2==7){
						System.out.println("What do you want your pawn to be promoted to? R/B/N/Q");
						Scanner in = new Scanner(System.in);
						String promo = in.nextLine();
						if(promo.equals("R")){
							theBoard[s1][s2] = new Rook('w');
						}else if(promo.equals("B")){
							theBoard[s1][s2] = new Bishop('w');
						}else if(promo.equals("N")){
							theBoard[s1][s2] = new Knight('w');
						}else{
							theBoard[s1][s2] = new Queen('w');
						}
						theBoard[s1][s2].setColor(true);
					}
				}
			}
			if(theBoard[s1][s2]!=null){
				if(theBoard[s1][s2].getName().equals("bp")){
					if(f2==0){
						System.out.println("What do you want your pawn to be promoted to? R/B/N/Q");
						Scanner in = new Scanner(System.in);
						String promo = in.nextLine();
							if(promo.equals("R")){
								theBoard[s1][s2] = new Rook('b');
							}else if(promo.equals("B")){
								theBoard[s1][s2] = new Bishop('b');
							}else if(promo.equals("N")){
								theBoard[s1][s2] = new Knight('b');
							}else{
								theBoard[s1][s2] = new Queen('b');
							}
							theBoard[s1][s2].setColor(false);
					}
				}
			
			}
			/**
			 * castling stuff
			 */
			Piece temp=null;
			if(theBoard[s1][s2]!=null){
				if(theBoard[s1][s2].getName().equals("wK")&&theBoard[s1][s2].getMoved()==false){
					if(theBoard[f1+1][f2]!=null){
						if(theBoard[f1+1][f2].getName().equals("wR")&&theBoard[f1+1][f2].getMoved()==false){
							skip =1;
							if(f1-s1>=1){
								theBoard[f1][f2]=theBoard[s1][s2];
								theBoard[f1-1][f2]=theBoard[f1+1][f2];
								theBoard[s1][s2]=null;
								theBoard[f1+1][f2]=null;
								int num = getCheckmate(whiteturn);
								if(num==1||num==2){
									theBoard[s1][s2]=theBoard[f1][f2];
									theBoard[f1][f2]=null;
									theBoard[f1+1][f2]=theBoard[f1-1][f2];
									theBoard[f1-1][f2]=null;
								}
							}else{
							}
						}
					}else if(theBoard[f1-2][f2]!=null){
						if(theBoard[f1-2][f2].getName().equals("wR")&&theBoard[f1-2][f2].getMoved()==false){
							System.out.println("Castling to the left \n");
							
							skip =1;
							if(f1-s1<=-1){
								theBoard[f1][f2]=theBoard[s1][s2];
								theBoard[f1+1][f2]=theBoard[f1-2][f2];
								theBoard[s1][s2]=null;
								theBoard[f1-2][f2]=null;
								int num = getCheckmate(whiteturn);
								if(num==1||num==2){
									theBoard[s1][s2]=theBoard[f1][f2];
									theBoard[f1][f2]=null;
									theBoard[f1-2][f2]=theBoard[f1+1][f2];
									theBoard[f1+1][f2]=null;
								}
							}
						}
					}
				}else if(theBoard[s1][s2].getName().equals("bK")&&theBoard[s1][s2].getMoved()==false){
					if(theBoard[f1+1][f2]!=null){
						if(theBoard[f1+1][f2].getName().equals("bR")&&theBoard[f1+1][f2].getMoved()==false){
							skip =1;
							if(f1-s1>=1){
								theBoard[f1][f2]=theBoard[s1][s2];
								theBoard[f1-1][f2]=theBoard[f1+1][f2];
								theBoard[s1][s2]=null;
								theBoard[f1+1][f2]=null;
								int num = getCheckmate(whiteturn);
								if(num==1||num==2){
									theBoard[s1][s2]=theBoard[f1][f2];
									theBoard[f1][f2]=null;
									theBoard[f1+1][f2]=theBoard[f1-1][f2];
									theBoard[f1-1][f2]=null;
								}
							}else{
							}
						}
					}else if(theBoard[f1-2][f2]!=null){
						if(theBoard[f1-2][f2].getName().equals("bR")&&theBoard[f1-2][f2].getMoved()==false){
							System.out.println("castling to the left \n");
							
							skip =1;
							if(f1-s1<=-1){
								theBoard[f1][f2]=theBoard[s1][s2];
								theBoard[f1+1][f2]=theBoard[f1-2][f2];
								theBoard[s1][s2]=null;
								theBoard[f1-2][f2]=null;
								int num = getCheckmate(whiteturn);
								if(num==1||num==2){
									theBoard[s1][s2]=theBoard[f1][f2];
									theBoard[f1][f2]=null;
									theBoard[f1-2][f2]=theBoard[f1+1][f2];
									theBoard[f1+1][f2]=null;
								}
							}
						}
					}
				}
			}
			/**
			 * cant move your piece if it puts you in check
			 * checks to see if you put the enemy in check/checkmate
			 */
			
			if(skip==0){
				temp = theBoard[f1][f2];
				theBoard[f1][f2] = theBoard[s1][s2];
				theBoard[s1][s2]= null;
				
				int checknum = getCheckmate(whiteturn);
					//if you went in check switch it back and return
				if(checknum==0){
					
					if(theBoard[f1][f2].getName().equals("wp")||theBoard[f1][f2].getName().equals("bp")){
						
						if(s1-f1==0&&(s2-f2==-2||s2-f2==2)){
							System.out.println("en passant \n");
							if(whiteturn){
								hiddenEnPass[s1]=1;
							}else{
								hiddenEnPass[s1+8]=1;
							}
						}
					}
					theBoard[f1][f2].move();
					return true;
				}
				else if(whiteturn && checknum <= 2){
					theBoard[s1][s2] = theBoard[f1][f2];
					theBoard[f1][f2] = temp;
					System.out.println("you cannot put yourself in check \n");
					return false;
					
				}
				else if(!whiteturn && checknum >=3){
					theBoard[s1][s2] = theBoard[f1][f2];
					theBoard[f1][f2] = temp;
					System.out.println("you cannot put yourself in check \n");
					return false;
				}
			}
			skip=0;
			return true;
				
		}
		else {
			System.out.println("you cannot kill your own pieces \n");
			return false;
		}
			
		
		
		
	
		
		
		
		//TODO stalemate
		
		
		
		
		
		
	}
	class Point{
		int x;
		int y;
	}
	
}
