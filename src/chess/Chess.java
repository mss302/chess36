package chess;
/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class Chess {

	public static void main(String[] args) {

		Board b = new Board();
		Scanner in = new Scanner(System.in);
		String move1="";
		String move2="";
		
		String inputline ="";
		boolean whiteturn = true;
		int error = 0;
		boolean drawflag = false;
		

		int nocheckmate = 0;
		boolean youredumb = false;
		
		
	
		/**
		 * main loop which quits if checkmate is reached
		 */
		while(nocheckmate==0 || nocheckmate==1 || nocheckmate==3 ){
			/**
			 * draw stuff
			 */
			if(drawflag==true){
				System.out.println("do you want to draw? yes/no");
				String answer = in.nextLine();
				if(answer.equals("yes")){
					System.out.println("game is a draw");
					System.exit(0);
				}else{
					System.out.println("game continues");
					drawflag = false;
					continue;
				}
			}
			
			System.out.println(b);
			System.out.println();
			if(whiteturn)
				System.out.print("White's move: ");
			else
				System.out.print("Black's move: ");
			inputline = in.nextLine();
			inputline.trim();
			/*StringTokenizer st = new StringTokenizer(inputline);
		    while (st.hasMoreTokens()) {
		         if(move1.equals("")){
		        	 move1 = st.nextToken();
		         }else if(move2.equals("")){
		        	 move2 = st.nextToken();
		         }else{
		        	 error = 1;
		        	 break;
		         }
		     }*/
			int spaceCount =0;
			for(int i =0;i<inputline.length();i++){
				if(inputline.charAt(i)==(' ')){
					spaceCount++;
				}
			}
			/**
			 * resign stuff
			 */
			if(spaceCount==0){
				if(inputline.equals("draw")){
					drawflag=true;
					continue;
				}else if(inputline.equals("resign")){
					if(whiteturn){
						System.out.println("Black wins the game!");
						System.exit(0);
					}else{
						System.out.println("White wins the game!");
						System.exit(0);
					}
				}else{
					System.out.println("Error not enough inputs \n");
					continue;
				}
			}else if(spaceCount==1){
				for(int i =0;i<inputline.length();i++){
					if(inputline.charAt(i)== (' ')){
						move1 = inputline.substring(0,i);
						move2 = inputline.substring(i+1,inputline.length());
						break;
					}
				}
			}else{
				System.out.println("Error too many inputs \n");
				continue;
			}
		    
			System.out.println(move1 +"" + move2);
			boolean v1= Pattern.matches("([a-h][1-8])", move1);
			//move2 = in.next();
			boolean v2= Pattern.matches("([a-h][1-8])", move2);
			//System.out.println(v1 && v2); //waits for 2 locations
			System.out.println();
			if(v1==false){
				System.out.println("Error in input1 \n");
				continue;
			}
			if(v2==false){
				System.out.println("Error in input2 \n");
				continue;
			}
			if(move1.equals(move2)){
				System.out.println("You cannot move a piece to its own location. \n");
			}else{
				int s1 = (int)move1.charAt(0) - 97;
				int s2 = (int)move1.charAt(1) - 49;	
				
				int f1 = (int)move2.charAt(0)-97;
				int f2 = (int)move2.charAt(1)-49;
				System.out.println(s1 + " " + s2 + " "+f1 + " "+f2);
				
				/**
				 * @success runs the board and finds legal moves
				 */
				
				boolean success = b.move(s1,s2,f1,f2,whiteturn);
				if(success ==true){
					nocheckmate = b.getCheckmate(whiteturn);
					if(nocheckmate==0){
						
					}
					else if(nocheckmate==1){
						System.out.println("White is in check \n");
						
					}
					else if(nocheckmate == 2){
						System.out.println("Checkmate! \n");
						System.out.println("The winner is white \n");
					}else if(nocheckmate ==3){
						System.out.println("Black is in check \n");
						
					}else{ 
						System.out.println("Checkmate! \n");
						System.out.println("The winner is black \n");
					}
					whiteturn = !whiteturn;
					move1="";
					move2="";
				}else{
					System.out.println("Invalid move. Try again. \n");
				}
			}
			
		}
		System.out.println("Game Over \n");
		

	}
	

}
