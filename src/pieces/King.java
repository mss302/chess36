package pieces;

import chess.Piece;

/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */

public class King extends Piece{
/**
 * 
 * @param color sets the name of the piece to wK or bK
 */
	public King (char color){
		this.setName(color + "K");
	}
	
	public boolean validMove(int s1, int s2, int f1, int f2) {
		/**
		 * @King can move in any direction 1 space and can castle
		 */
		
		if(s1==f1){						//check if moving in same row
			if(s2-f2==1||s2-f2==-1){
				return true;
			}
		}
		
		else if(s2==f2){				//check if moving in same column
			
			if(s1-f1==1||s1-f1==-1){
				return true;
			}else if((s1-f1==2||s1-f1==-2)&&getMoved()==false){
				
				
				return true;
					
			}
		}
		
		else{							//check if moving 1 diagonally
			if((s1-f1==1||s1-f1==-1)&&(s2-f2==1||s2-f2==-1)){
				return true;
			}
		}
		
		return false;
	}

}
