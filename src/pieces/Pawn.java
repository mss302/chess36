package pieces;

import chess.Piece;

/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */
public class Pawn extends Piece{
/**
 * 
 * @param color sets the name to wp or bp
 */
	public Pawn (char color){
		this.setName(color + "p");
	}
	
	public boolean validMove(int s1, int s2, int f1, int f2) {
		/**
		 * @pawn cant go backwards, can go 2 spaces on first move, can go diagonal to kill or en passant.
		 */
		if(this.getName().charAt(0)=='w'){	//cant go backwards
			if(f2-s2<0){
				
				return false;
			}
		}else{
			if(f2-s2>0){
				
				return false;
			}
		}
		
		
		if(this.getMoved()==false){	//can go 2 spaces on its first move
			if(s1==f1&&(s2-f2==2||s2-f2==-2)){
				
				return true;
			}else if(s1==f1&&(s2-f2==1||s2-f2==-1)){
				

				return true;
			}
		}else{
			if(s1==f1&&(s2-f2==1||s2-f2==-1)){
				

				return true;
			}
		}
		
		if(s2-f2==1||s2-f2==-1){
			
		
			if(this.getName().charAt(0)=='w'){
				if(f1-s1==1||f1-s1==-1){
					

					return true;
				}
			}else{
				
				if(s1-f1==-1||s1-f1==1){
					

					return true;
				}
			}
		}else{
			//can go diagonally if it is trying to capture
			return false;
		}
		
	
		
		return false;
	}

}
