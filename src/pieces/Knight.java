package pieces;

import chess.Piece;

/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */
public class Knight extends Piece{
/**
 * 
 * @param color sets the name of the piece to wN or bN
 */
	public Knight(char color){
		this.setName(color + "N");
	}
	
	public boolean validMove(int s1, int s2, int f1, int f2) {
		/**
		 * @Knight can move 2 spaces in one direct and 1 in perpendicular direction
		 * can hop over pieces
		 */
		
		if(s1-f1==2||s1-f1==-2){	//move up or down 2 spaces
			if(s2-f2==1||s2-f2==-1){	//move left or right 1 space
				return true;
			}
		}
		
		if(s2-f2==2||s2-f2==-2){	//move right or left 2 spaces
			if(s1-f1==1||s1-f1==-1){	//move up or down 1 space
				return true;
			}
		}
		
		return false;
	}

}
