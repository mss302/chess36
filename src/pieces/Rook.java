package pieces;

import chess.Piece;

/**
 * 
 * @author Ben Bancala and Michael Shafran
 *
 */
public class Rook extends Piece{
/**
 *  
 * @param color sets name to bR or wR
 */
	public Rook (char color){
		this.setName(color + "R");
	}
	public boolean validMove(int s1, int s2, int f1, int f2) {
		/**
		 * @Rook can move up down or left right x amount of spaces
		 * cannot go diagonal
		 */
		
		if(s1==f1&&s2!=f2){	//same row. can move any amount of spaces.
			return true;
		}else if(s1!=f1&&s2==f2){	//same column. can move any amount of spaces.
			return true;
		}
		
		return false;
	}

}
